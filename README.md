# Alura Certificates Download

Script para fazer o download de PDFs de certificados e gerar lista de certificados e carga horária a partir de uma lista de URLs.

- Salva os arquivos PDF dos certificados válidos em /certificados/*
- Certificados válidos: 8h por dia de duração do curso
- Lista os cursos e carga horária em /certificados/certificados.txt                              
- Mostra os certificados inválidos na saída do console

## Requisitos

- python3, python3-pip

## Instalação

```
cd alura-certificates-download
pip3 install pyhtml2pdf
```

## Execução

1. Colocar as URLs para os certificados em urls[]. Ex:
```
urls = [
    'https://cursos.alura.com.br/certificate/97da80fe-f86c-442b-abb7-dfe21d593aa5',
    'https://cursos.alura.com.br/certificate/c241abce-ffb9-448d-ab26-47f6f975be82'
    ]
```

2. Salvar e executar o script

```
path/to/python scrap.py
```
Ex Windows:
```
C:\Users\user\AppData\Local\Programs\Python\Python311\python.exe scrap.py
```
Ex Linux:
```
python3 scrap.py
```

## Autores
- Guilherme Moreira

v. 0.0.1