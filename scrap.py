# Script para baixar automaticamente os certificados da Alura           |
# Autor: Guilherme Moreira (guilhermeribmoreira@gmail.com)              |
# v. 0.0.1 12/01/2023                                                   |
# ----------------------------------------------------------------------|
# Salva os arquivos PDF dos certificados válidos em /certificados/*     |
# Certificados válidos: 8h por dia de duração do curso                  |
# Lista os cursos e carga horária em /certificados.txt                  |                                  
# Mostra os certificados inválidos                                      |
# requisitos: python3, python3-pip, pyhtml2pdf                          |
# Execução: path_to_python/python3.exe scrap.py                         |
# ======================================================================|

import os
import re
import requests as r
from datetime import datetime
from pyhtml2pdf import converter

HR_DIA = 8

urls = [
    'https://sample1.com',
    'https://sample2.com',
]
   

def extract_html_info(text: str) -> dict:
    result = re.search(r'curso online "(.*)" .* em (\d+) horas, .* período de (\d+\/\d+\/\d+) a (\d+\/\d+\/\d+).', text)
    nome = result.group(1)
    horas = result.group(2)
    dt_inicial = datetime.strptime(result.group(3), "%d/%m/%Y").date()
    dt_final = datetime.strptime(result.group(4), "%d/%m/%Y").date()

    dt_diff = (dt_final - dt_inicial).days

    is_valid = (dt_diff+1)*HR_DIA >= int(horas)

    return {'nome': nome, 'nome_limpo': re.sub(r"[^A-Za-zçÇáÁàêãõ]+", '_', nome), 'horas': horas, 'is_valid': is_valid}

def save_certificates():

    if not os.path.exists("certificados"):
        os.mkdir("certificados")

    total_de_horas = 0

    for item in urls:

        response = r.get(item)
        html_text = response.text
        url = "https://cursos.alura.com.br" + re.search(r'href="(.*)" class', html_text).group(1)

        formal_certificate_page = r.get(url)

        certificate_infos = extract_html_info(formal_certificate_page.text)

        if certificate_infos['is_valid']: 
            converter.convert(url, f"certificados/{certificate_infos['nome_limpo']}.pdf")

            with open("certificados/certificados.txt", "a") as f:
                f.write(f"{certificate_infos['nome'].upper()} - {certificate_infos['horas']} hrs\n")
        
            print(f"Certificado: {certificate_infos['nome'].upper()} - {certificate_infos['horas']} hrs | salvo com sucesso")

            total_de_horas += int(certificate_infos['horas'])

        else:
            print(f"Certificado INVÁLIDO: {certificate_infos['nome'].upper()} - {certificate_infos['horas']} hrs")

        
    with open("certificados/certificados.txt", "a") as f:
            f.write(f"---\nTotal de horas: {total_de_horas} horas")


if __name__ == "__main__":
    save_certificates()
